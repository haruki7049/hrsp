use clap::Parser;
use linefeed::{Interface, ReadResult};

#[derive(Debug, Parser)]
pub struct Args {
    pub subcommands: SubCommands,
}

#[derive(Debug, Clone)]
pub enum SubCommands {
    Repl,
}

impl std::str::FromStr for SubCommands {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "repl" => Ok(Self::Repl),
            _ => Err(format!("Unknown subcommand: {}", s)),
        }
    }
}

pub fn repl(prompt: &'static str) {
    let mut interface = Interface::new(prompt).unwrap();
    interface.set_prompt("hrsp:> ").unwrap();

    while let Ok(ReadResult::Input(line)) = interface.read_line() {
        println!("You typed: {}", line);
    }
}
