use clap::Parser;
use hrsp::Args;

const PROMPT: &str = "hrsp:> ";

fn main() {
    let args = Args::parse();
    match args.subcommands {
        hrsp::SubCommands::Repl => hrsp::repl(PROMPT),
    }
}
