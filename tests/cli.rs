//! This file contains the tests for the CLI.

use assert_cmd::Command;
use std::process::Output;

#[test]
fn test_cli_help() {
    let output: Output = Command::cargo_bin("hrsp").unwrap().arg("--help").unwrap();
    assert!(output.status.success());
}
